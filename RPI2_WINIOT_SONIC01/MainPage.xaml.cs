﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Gpio;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace RPI2_WINIOT_SONIC01
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        Stopwatch sw_main = new Stopwatch();
        public MainPage()
        {
            // this.InitializeComponent();

            GpioController gpio = GpioController.GetDefault();

            GpioPin echoPin = gpio.OpenPin(23);
            GpioPin triggerPin = gpio.OpenPin(24);
            UltraSonicSensor us = new UltraSonicSensor(triggerPin, echoPin);
            while (true)
            {

                //Task.Delay(500).Wait();

                sw_main.Start();
                while (sw_main.Elapsed.TotalMilliseconds < 500) ;
                sw_main.Reset();
                us.Timing();

                System.Diagnostics.Debug.WriteLine("RUN");
            }

        }


    }

    public sealed class UltraSonicSensor
    {

        GpioPin _triggerPin;
        GpioPin _echoPin;

        Stopwatch sw0 = new Stopwatch();


        public UltraSonicSensor(GpioPin triggerPin, GpioPin echoPin)
        {
            triggerPin.SetDriveMode(GpioPinDriveMode.Output);
            echoPin.SetDriveMode(GpioPinDriveMode.Input);

            _triggerPin = triggerPin;
            _echoPin = echoPin;
        }


        DateTime now;
        Stopwatch pulseLength = new Stopwatch();
        public bool Timing()
        {
            bool _isNear = false;

            pulseLength.Reset();
            sw0.Reset();

            Debug.WriteLine("send signal ");

            //Task.Delay(2).Wait();

            sw0.Start();
            while (sw0.Elapsed.TotalMilliseconds < 2) ;
            sw0.Reset();

            _triggerPin.Write(GpioPinValue.Low);
            //Task.Delay(TimeSpan.FromMilliseconds(500)).Wait();

            sw0.Start();
            while (sw0.Elapsed.TotalMilliseconds < 500) ;
            sw0.Reset();

            _triggerPin.Write(GpioPinValue.High);

            now = DateTime.Now;
            // Task.Delay(TimeSpan.FromMilliseconds(.01)).Wait();

            sw0.Start();
            while (sw0.Elapsed.TotalMilliseconds < 0.01) ;
            Debug.WriteLine("Trigger Pulse length: " + sw0.Elapsed.TotalMilliseconds);
            sw0.Reset();

            _triggerPin.Write(GpioPinValue.Low);

            while (_echoPin.Read() == GpioPinValue.Low) ;
            pulseLength.Start();


            while (this._echoPin.Read() == GpioPinValue.High) ;
            pulseLength.Stop();

            //Calculating distance
            TimeSpan timeBetween = pulseLength.Elapsed;
            //Debug.WriteLine("time diff : " + timeBetween.ToString());
            double distance = timeBetween.TotalMilliseconds * 34.029 / 2.0;
            Debug.WriteLine("Distance(cm): " + distance.ToString());

            if (distance < 10)
            {
                _isNear = true;
            }

            return _isNear;

        }


    }
}
